#!/usr/local/bin/python2.7

import time

def euler30(exponent):
  """
  Find the sum of all the numbers that can be written 
  as the sum of fifth powers of their digits
  """
  
  if exponent <= 1:
    return "The exponent must be at least 2."

  power_digits = [i ** exponent for i in range(10)]
  total_sum = 0
  upper_bound = (exponent + 1) * power_digits[9]
  for number in range(10, upper_bound + 1):
    partial_sum = temp_num = number
    while temp_num:
      partial_sum -= power_digits[temp_num % 10]
      temp_num /= 10
    if not partial_sum:
      total_sum += number

  return total_sum

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler30(5))
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
