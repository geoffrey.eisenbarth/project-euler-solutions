"""
<p>
<strong>Statement: </strong> \(215 = 32768\) and the 
sum of its digits is \(3 + 2 + 7 + 6 + 8 = 26\). What 
is the sum of the digits of the number \(21,000\)?
</p>
<hr />
<p>This problem is quick to solve in Python using built-in
functions and list comprehensions. Using Python's exponentiation
notation, we will first take \(2^1000\) and convert it to a string:
</p>
<pre style="text-align: center;">str(2 ** 1000)</pre>
<p>
The reason we need to convert this number to a string is that strings
in Python are <em>iterable</em> and we can loop through them from left
to right, character by character. We wish to create a list containing 
each of the digits in this number so that we can use the <tt>sum()</tt>
function on this list. We will iterate through each character in the 
number, converting it back from a string to an integer:
</p>
<pre style="text-align: center;">[int(character) for character in str(2 ** 1000)].</pre>
<p>
Now we have a list containing all the digits of the number \(2^1000\), and we can simply
sum it. This is done below.
<p>
<pre><code>
"""
import time

def euler():
  return sum([int(char) for char in str(2 ** 1000)])
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()

"""
</code></pre>
<hr>
<pre>
The solution is: 1366
Time elapsed: 0.000999
</pre>
"""