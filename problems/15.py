"""
<p>
<strong>Statement: </strong> Starting in the top left corner of a 
\(2 \times 2\) grid, and only being able to move to the right and 
down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a \(20\times 20\) grid?
</p>
<hr>
<p>
The solution to the problem above can be seen in the following picture:
<figure>
  <img src="https://projecteuler.net/project/images/p015.gif" align="center">
  <figcaption style="font-size: 75%;">
  The six solution paths for a \(2 \times 2\) grid
  <br>
  Source: Project Euler
  </figcaption>
</figure>
Notice that every path to the bottom right corner under our restrictions
(only moving right and down) requires 2 moves to the right and 2 moves 
down. Therefore, we can associate a four letter 'word' with every solution
comprised of the letters 'R' and 'D'. Viewing the images from right to left,
top to bottom, we have:
</p>
<ul>
<li> RRDD </li>
<li> RDRD </li>
<li> RDDR </li>
<li> DRRD </li>
<li> DRDR </li>
<li> DDRR </li>
</ul>
<p>Therefore, we can solve the \(20 \times 20\) case by appealing to this
logic: how many unique 40 letter words are there comprised of 20 'R's and
20 'D's? Notice, it is important to investigate <em>unique</em> combinations
of our letters, since the solution given by 'RRDD' and the solution given by 
switching the first two letters/moves of 'RRDD' result in the same path. The
mathematics of permutations and combinations is known as <a href="http://en.wikipedia.org/wiki/Combinatorics>
Combinatorics</a>, and we can use some combinatorical results to quickly
solve our problem.
</p>
<p>
I'm going to prove my claim using the \(2 \times 2\) example above for the 
sake of brevity, and then we'll apply it to the \(20 \times 20\) problem
in our code. If we want to know how many unique, four-letter arrangements
of two letters there are, it is best to first determine how many unique, 
four-letter arrangements of four letters ('A', 'B', 'C', and 'D') exist. 
Let's consider the four possible spaces for the letters:
</p>
<p style="text-align: center">__  __  __  __</p>
<p>
In the first space, we have four options: either place an 'A', 'B', 'C', or
'D' there. However, once I choose a starting letter, I only have three 
possible choices for the second letter. That is, each of my initial four
possibilities each have three possible second letter choices. In just dealing
with the first two letters, we have \(4 \times 3\) possibilities. Similarly, 
once the first two letters have been decided, there will only remain two 
choices for the third letter, and one letter left over for slot four. So 
each of the four possible first letter choices results in three different 
second letter choices, which each result in two different third letter result,
leaving one letter left for the fourth letter. Therefore, there are \(4 \times
3 \times 2 \times 1\) possible words. Mathematically, this is called a 
<em>factorial</em> and we notate factorials as
\[n! := n \times (n-1) \times (n-2) \times \cdots \times 3 \times 2 \times 1,\]
where the symbol \(:=\) means "is defined to be equal to." 
</p>
<p>
However, it won't work for us to just calculate \(40!\) as our solution. 
This is because we want unique combinations of only two letters. Thus, in 
our \(2 \times 2\) example, we need to divide the number of possible words
by the different ways it's possible for two words to be the same. For 
instance, if we switch the middle two letters in 'RDDR', we still get 'RDDR'.
How many different ways are there to organize the two 'D's in the middle?
Since have two letters (even though they're the same value) and a length of
two, there are \(2! = 2 \times 1) ways to organize the 'R's. Notice that 
all of these ways of organizing the 'R's lead to the same solution to our
problem. The same can be said of the 'D's, in that there are \(2!\) ways
to organize them in a given solution that will result in the same path. 
Therefore, we divide our total number of possible words by \(2!\) twice
(once for 'R' and once for 'D') and we obtain
\[\frac{4!}{2!\times 2!} = \frac{4 \times 3 \times 2 \times 1}{(2\times 1)(2\times 1)}=\frac{24}{4}=6,\]
the answer to the \(2 \times 2\) problem.
</p>
<p>
This should be easy enough to implement in Python, as the code below
demonstrates. In order to quickly calculate factorials, we import the
factorial function from the math module. Taking what we learned from the
\(2 \times 2\) example, we need to compute \(\frac{(2 \times 20)!}{20!\times 20!}:\)
</p>
<pre><code>
"""
from math import factorial
import time

def euler15():
  square_dimension = 20 
  return factorial(2 * square_dimension) / (factorial(square_dimension) ** 2)
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler15())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 137846528820
Time elapsed: 0.012000
</pre>
"""
