<p>
<strong>Statement: </strong> If the numbers 1 to 5 are written out 
in words: one, two, three, four, five, then there are 
\[3 + 3 + 5 + 4 + 4 = 19\]
letters used in total. If all the numbers from 1 to 1000 (one thousand) 
inclusive were written out in words, how many letters would be used?
</p>
<p>
Note: Do not count spaces or hyphens. For example, 342 (three hundred 
and forty-two) contains 23 letters and 115 (one hundred and fifteen) 
contains 20 letters. The use of "and" when writing out numbers is in 
compliance with British usage.
</p>
<hr />
<p>
To solve this problem, we will take advantage of the Python dictionary
data structure. Dictionaries provide an convenient and quick way to 
map correspondences in programs. The basic use is as follows:
<p>
<pre style="text-align: center;">
dictionary = { key1 : value1, key2 : value2 }
</pre>
<p>
This will store the <tt>(key, value)</tt> values in memory, and allow
us to quickly fetch them when needing using the following syntax: <tt>
dictionary[key2]</tt> will return the value <tt>value2</tt>.
</p>
<p>
We will create a dictionary that takes as key values integers from
one to one thousand, and returns a string of the spelled out number.
Some of these numbers have to be hard-wired in, but to save us time
most of the numbers will be procedurally generated using previously
entered entries in the dictionary. 
</p>
<pre><code>
import time
from itertools import product

def create_word_correspondence():

  """
  Creates a dictionary to allow us to convert from
  integers to words. We manually key in the core words,
  since everything else can be  procedurally generated.
  """
  dict = { 
          1    : 'one',
          2    : 'two', 
          3    : 'three',
          4    : 'four',
          5    : 'five', 
          6    : 'six',
          7    : 'seven',
          8    : 'eight',
          9    : 'nine', 
          10   : 'ten',
          11   : 'eleven',
          12   : 'twelve',
          13   : 'thirteen',
          14   : 'fourteen',
          15   : 'fifteen',
          16   : 'sixteen',
          17   : 'seventeen',
          18   : 'eighteen',
          19   : 'nineteen',
          20   : 'twenty', 
          30   : 'thirty',
          40   : 'forty',
          50   : 'fifty',
          60   : 'sixty',
          70   : 'seventy',
          80   : 'eighty',
          90   : 'ninety',
          1000 : 'one thousand'
         }                    

  """
  We procedurally generate the the tens (e.g., 'thirty one')
  """
  for i, j in product(range(2, 10), range(1, 10)):
    dict[int(str(i) + str(j))] = dict[i * 10] + ' ' + dict[j]
    
  """
  Generate the hundreds
  """
  for k in range(1, 10):
    # e.g., 'four hundred'
    dict[k * 100] = dict[k] + ' hundred'
  
    # e.g., 'five hundred and eleven'
    for i in range(1, 20):
      dict[k * 100 + i] = dict[k * 100] + ' and ' + dict[i]
     
    # e.g., 'three hundred and thirty seven'
    for i, j in product(range(2, 10), range(0, 10)):
      dict[k * 100 + i * 10 + j] = dict[k * 100] + ' and ' + dict[i * 10 + j]

  return dict
</code></pre>
<p>
Now that the dictionary-creating logic has been instituted, we just 
to create a string that contains every value in the dictionary (recall,
'keys' refer to the inputs which are integers in our case, and 'values'
refer to the outputs, or the spelled out words). We replace any spaces
per the statement of the problem, then count how many letters are in 
the resulting string.
</p>
<pre><code>
def euler():
  """
  Create the dictionary, join all numbers less than or equal to 1000 
  (i.e., all the values in the dictionary) into one string, and omit 
  any spaces. We then return the length of this string.
  """
  int_to_word = create_word_correspondence()
  all_numbers_string = ''.join(int_to_word.values()).replace(' ', '')
  
  return len(all_numbers_string)
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
</code></pre>
<hr>
<pre>
The solution is: 21124
Time elapsed: 0.002000
</pre>
