#!/usr/local/bin/python2.7

import time
 
def euler48():
  """
  The series, 1 ** 1 + 2 ** 2 + 3 ** 3 + ... + 10 ** 10 = 10405071317.

  Find the last ten digits of the series, 
  1 ** 1 + 2 ** 2 + 3 ** 3 + ... + 1000 ** 1000.
  """
  digits = []
  for i in range(1, 1001):
    digits.append(int(str(i ** i)[-10:]))
  
  return int(str(sum(digits))[-10:])

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler48())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
