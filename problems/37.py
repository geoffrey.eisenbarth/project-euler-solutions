#!/usr/local/bin/python2.7

import time
import math
import itertools
import re

def is_prime(n):

  #special cases 2 and 3, excludes (-infty, 1]
  if n <= 3:
    return n >= 2  

  #Only need to check for divisors less than the square-root
  ubound = int(math.sqrt(n)) + 1
  #Since all primes except 2 and 3 are of the form p = 6k +/- 1, 
  #we look for prime divisors of this form. Note, that just because 
  #a number has the form 6k +/- 1 does not mean it will be prime, but
  #we are sure to pick up all primes less than ubound with this
  #approach. 
  
  if n % 2 == 0 or n % 3 == 0 or n % 5 == 0:
    return False
    
  k = 1
  while 6 * k + 1 <= ubound:
    if n % (6 * k + 1) == 0 or n % (6 * k - 1) == 0:
      return False
    k += 1
  
  return True
  
def is_truncatable(number):
  for index in range(1, len(str(number))):
    if not is_prime(int(str(number)[index:])) or not is_prime(int(str(number)[:index])):
      return False
  return True
    
def euler37():
  """
  The number 3797 has an interesting property. Being prime itself, it is possible 
  to continuously remove digits from left to right, and remain prime at each 
  stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 
  379, 37, and 3.

  Find the sum of the only eleven primes that are both truncatable from left to 
  right and right to left.

  NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
  """
  truncatable_primes = [11]
  n, f = 11, 1
  while len(truncatable_primes) < 11:
    n += 3 - f #fast count for prime candidates
    f = -f
    if not (n > 100 and re.search('[245680]', str(n))):
      if is_prime(n) and is_truncatable(n):
        truncatable_primes.append(n)

  return sum(truncatable_primes)    
    
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler37())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
