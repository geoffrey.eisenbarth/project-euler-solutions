import time

def euler6():
  return sum(range(101)) ** 2 - sum([i ** 2 for i in range(101)])
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler6())
  elapsed = (time.time() - start)
  print('Time elapsed: %s seconds' % str(elapsed))

if __name__ == '__main__':
  main()