#!/usr/local/bin/python2.7

import time
from math import sqrt

def word_to_number(word):
  letter_value = {'A' : 1, 'B' : 2, 'C' : 3, 'D' : 4, 'E' : 5, 
                  'F' : 6, 'G' : 7, 'H' : 8, 'I' : 9, 'J' : 10,
                  'K' : 11, 'L' : 12, 'M' : 13, 'N' : 14, 'O' : 15,
                  'P' : 16, 'Q' : 17, 'R' : 18, 'S' : 19, 'T' : 20,
                  'U' : 21, 'V' : 22, 'W' : 23, 'X' : 24, 'Y' : 25, 
                  'Z' : 26}

  return sum([letter_value[letter] for letter in word.replace('"', '')])
  
def is_triangular(number):
  """
  Given that triangular numbers satisfy t_n = (1/2) n * (n + 1), 
  we can check if a given number is triangular by determining if
  n ** 2 + n - 2 t_n = 0 has a solution. Using the quadratic equation,
  and assuming the solution n > 0, we just need to check if
  (-1 + sqrt(1 + 8 t_n) ) / 2 is an integer
  """
  return ((-1.0 + sqrt(1.0 + 8.0 * number)) / 2.0).is_integer()
  
def euler42():
  """"
  The nth term of the sequence of triangle numbers is given by
  t_n = (1/2)n(n+1); so the first ten triangle numbers are:

  1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

  By converting each letter in a word to a number corresponding to its 
  alphabetical position and adding these values we form a word value. 
  For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the 
  word value is a triangle number then we shall call the word a triangle word.

  Using words.txt, a 16K text file containing nearly two-thousand common 
  English words, how many are triangle words?
  """
  
  with open('p042_words.txt', 'r+') as file:
    word_bank = [word for word in file.read().split('","')]
  triangle_words = [word for word in word_bank if is_triangular(word_to_number(word))]
  return len(triangle_words)

    
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler42())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
