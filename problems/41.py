#!/usr/local/bin/python2.7

import time
import math
from itertools import permutations

def is_prime(n):

  #special cases 2 and 3, excludes (-infty, 1]
  if n <= 3:
    return n >= 2  
  
  #Only need to check for divisors less than the square-root
  ubound = int(math.sqrt(n)) + 1
  
  #Since all primes except 2 and 3 are of the form p = 6k +/- 1, 
  #we look for prime divisors of this form. Note, that just because 
  #a number has the form 6k +/- 1 does not mean it will be prime, but
  #we are sure to pick up all primes less than ubound with this
  #approach. 
  
  if n % 2 == 0 or n % 3 == 0 or n % 5 == 0:
    return False
   
  k = 1
  while 6 * k + 1 <= ubound:
    if n % (6 * k + 1) == 0 or n % (6 * k - 1) == 0:
      return False
    k += 1
  
  return True
  
def is_pandigital(number, ubound):
  input_digits = sorted([int(digit) for digit in str(number)])
  test_digits = sorted([digit for digit in range(1, ubound + 1)])
  return input_digits == test_digits

def euler41():
  """"
  A number is divisible by 3 if and only if the sum of its digits
  is. Taking this in consideration, every n-pandigitial number 
  except for 4 and 7 digit ones will be divisible by 3, so we
  remove those possibilities.
  """
  candidates = []
  for i in range(2):
    if i == 0:
      digits = ['1', '2', '3', '4', '5', '6', '7']
    else:
      digits = ['1', '2', '3', '4']
     
    for permutation in permutations(digits):
      candidate = int(''.join(permutation))
      #pandigitals are rarer than primes, so we utilize short-circuiting
      if is_pandigital(candidate, len(digits)) and is_prime(candidate):
        candidates.append(candidate)

  return sorted(candidates)[-1]
    
    
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler41())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
