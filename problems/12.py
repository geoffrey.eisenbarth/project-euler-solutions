<p>
<bold>Statement: </bold> The sequence of triangle numbers is generated 
by adding the natural numbers. So the 7th triangle number would be 
\(1 + 2 + 3 + 4 + 5 + 6 + 7 = 28.\) The first ten terms of this sequence
would be:

\[1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...\]

Let us list the factors of the first seven triangle numbers:
</p>
<pre>
 1: 1
 3: 1, 3
 6: 1, 2, 3, 6
10: 1, 2, 5, 10
15: 1, 3, 5, 15
21: 1, 3, 7, 21
28: 1, 2, 4, 7, 14, 28
</pre>

28 = 1, 2, 2, 7, 28
<p>
We can see that 28 is the first triangle number to have over five divisors. 
What is the value of the first triangle number to have over five hundred 
divisors?
</p>
<hr>
<p>
This problem is a bit more exciting mathematically than the previous one. Since we
wish to count the divisors of triangular numbers, we first craft two functions: one to 
return the \(n^{\mbox{th}}\) triangular number and one to return a number's prime
factorization. As I'll show later, determining how many divisors a given number has can be
done efficiently if you know the prime factors.
</p>
<p>
Since triangular numbers are the sum of the first \(n\) digits, we can calculate
them quickly using the formula described in my <a href="http://dr-ironbeard.tumblr.com/post/119448206255/project-euler-problem-1">
solution</a> to the first Euler problem:
</p>
<pre><code>
def triangle(n):
  """
  The nth triangle number is the sum from 1 to n. 
  Quickly calculated using  Gauss' formula.
  """
  return (n * (n + 1)) / 2
</code></pre>
<p>
Additionally, I modified the <tt>prime_factors()</tt> function 
from my <a href="http://dr-ironbeard.tumblr.com/post/119459046750/project-euler-problem-3">
solution</a> to Project Euler Problem 3. For convenience, I decided to store the
factorization in a dictionary of the form {prime factor : exponent}. I used the 
<tt>defaultdict</tt> class from from <tt>collections</tt> so that I didn't have to
initialize the value of every key I put in the dictionary. 
<pre><code>
from collections import defaultdict

def prime_factors(n):
  i = 2 
  factors = defaultdict(int)
  
  while i * i <= n:     # only need to test up to the square root of n
    if n % i:           # if n not divisible by i 
      i += 1            #     increment and move on  
    else:               # if n is divisble by i
      n //= i           #     divide n by i
      factors[i] += 1   #     increment the exponent associated with i
                        #
  if n > 1:             # at this point n is a prime number itself
    factors[n] += 1     #     so increment that exponent
    
  return factors 
</code></pre>
<p>
Now I wish to take a minute to explain how to find the number of divisors
a particular number \(n\) has in terms of it's prime factorization 
\[n = p_1^{e_1} \times p_2^{e_2} \times \dots \times p_m^{e_m},\]
where the \(p_i\) are prime. 
</p>
<p>
We denote by \(d(\cdot):\mathbb{N} \to \mathbb{N}\) the function that takes
as an input a natural number \(n\) and outputs the number of divisors of \(n\),
also a natural number (note: the set of all natural number is denoted \(\mathbb{N}\).)
By definition, it must be that \(d(p) = 2\) for all prime numbers \(p\), since
the only divisors of \(p\) would be \(1\) and \(p\). Similarly, given a prime
\(p\) and an exponent \(e\), the divisors of \(p^e\) would be
\[1, p, p^2, p^3, \dots, p^e,\]
so we have that \(d(p^e) = (e + 1)\). 
</p>
<p> 
The final step in our divisor analysis will be to show that the divisor 
function \(d(\cdot)\) is multiplicative. A function \(f(\cdot)\)
is said to be <em>multiplicative</em> (or a <em>homomorphism</em> to borrow 
language from group theory) if \(f(a \times b) = f(a) \times f(b)\) or any
values of \(a\) and \(b\). Let \(p_1\) and \(p_2\) be primes, and \(e_1\), 
\(e_2\) arbitrary but fixed natural numbers. Notice that the table below
lists all the divisors of \(n = p_1^{e_1} \times p_2^{e_2}\).
</p>
<table>
<tr>
  <td>\(1\)</td>
  <td>\(p_1\)</td>
  <td>\(p_1^2\)</td>
  <td>\(\dots\)</td>
  <td>\(p_1^{e_1}\)</td>
</tr>
<tr>
  <td>\(p_2\)</td>
  <td>\(p_1 p_2\)</td>
  <td>\(p_1^2 p_2\)</td>
  <td>\(\dots\)</td>
  <td>\(p_1^{e_1} p_2\)</td>
</tr>
<tr>
  <td>\(p_2^2\)</td>
  <td>\(p_1 p_2^2\)</td>
  <td>\(p_1^2 p_2^2\)</td>
  <td>\(\dots\)</td>
  <td>\(p_1^{e_1} p_2^2\)</td>
</tr>
<tr>
  <td>\(\dots\)</td>
  <td>\(\dots\)</td>
  <td>\(\dots\)</td>
  <td>\(\dots\)</td>
  <td>\(\dots\)</td>
</tr>
<tr>
  <td>\(p_2^{e_2}\)</td>
  <td>\(p_1 p_2^{e_2}\)</td>
  <td>\(p_1 p_2^{e_2}\)</td>
  <td>\(\dots\)</td>
  <td>\(p_1^{e_1} p_2^{e_2}\)</td>
</tr>
</table>
<p>
Multiplying the number of rows times the number of columns, we can see that
\[d(n) = d(p_1^{e_1} \times p_2^{e_2}) = (e_1 + 1)(e_2 + 2) = d(p_1^{e_1}) 
\times d(p_2^{e_2}).\]
As a result, we can utilize a pretty efficient <tt>count_divisors()</tt> 
function, although it's speed will be dependent on the speed in which the
prime factorization of its input can be calculated. If you're unfamiliar with
either the <tt>reduce()</tt> built-in function or <tt>lambda</tt> functions,
I covered them briefly in my <a href="http://dr-ironbeard.tumblr.com/post/120023223260/project-euler-problem-11">
solution</a> to Euler problem 11.
</p>
<pre><code>
def count_divisors(n):
  """
  Given a prime factorization n = (p1^e1) * (p2^e2) * ... * (pm^em), the
  number of factors is (e1 + 1)(e2 + 1)...(em + 1).
  """
  return reduce(lambda x, y: x * y, [exponent + 1 for exponent in prime_factors(n).values()])
</code></pre>
<p>
Now that all the heavy lifting has been done, we can easily implement a loop
until we find a triangular number with more than 500 factors. 
</p>
<pre><code>
def euler12():
  n = 2
  while count_divisors(triangle(n)) < 500:
    n += 1
  return triangle(n)

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler12())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
</code></pre>
<hr>
<pre>
The solution is: 76576500
Time elapsed: 0.633999 
</pre>
