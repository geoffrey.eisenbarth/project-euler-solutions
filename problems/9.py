#!/usr/local/bin/python2.7

import sys
import math
import time

def euler9():
  """
  Solution to Euler9: There exists exactly one Pythagorean triplet for which a + b + c = 1000.
  Find the product abc.
  """
import math 

def euler9():
    """
    The given constraints a + b + c = 1000 and a ** 2 + b ** 2 = c ** 2 are equivalent to both
    b = sqrt(c ** 2 - a ** 2) and c = (1000^2 - 2000 * a + 2* a ** 2)/(2000 - 2 * a)
    """
    
    a = 1
    while True:
      b = 1000.0 * (a - 500.0) / (a - 1000.0)
      c = (500000 - 1000.0 * a + a ** 2.0)/(1000.0 - * a) 
      
      if (b.is_integer() and c.is_integer()) and 0 < b < c:
        print a, b, c
        return a * b * c
      if a == 999:
        a += 2
      else:
        a+=1
       

#boilerplate code to run
def main():
  start = time.time()
  print 'The solution is %d' % euler9()
  print 'Time elapsed: ' + str((time.time() - start))
  
if __name__ == '__main__':
  main()
