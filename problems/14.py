"""
<p>
<strong>Statement: </strong>
The following iterative sequence is defined for the set of positive 
integers:

\[n \to \frac{n}{2} \qquad (n \mbox{ is even})\]
\[n \to 3n + 1 \qquad (n \mbox{ is odd})\]

Using the rule above and starting with 13, we generate the following 
sequence:

\[13 \to 40 \to 20 \to 10 \to 5 \to 16 \to 8 \to 4 \to 2 \to 1.\]

It can be seen that this sequence (starting at 13 and finishing at 1) 
contains 10 terms. Although it has not been proved yet (Collatz Problem), 
it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

Note: Once the chain starts the terms are allowed to go above one million.
</p>
<hr>
<p>
As mentioned above, the <a href='http://en.wikipedia.org/wiki/Collatz_conjecture'>
Collatz Conjecture</a> is still unproven, regardless of first being proposed
in 1937. Paul Erdos famously said that "Mathematics may not be ready for 
such problems" when discussing the conjecture. Despite several claims to 
have found a proof, each potential proof has contained flaws as of this 
writing. 
</p>
<figure>
  <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/2/26/CollatzStatistic100million.png/1920px-CollatzStatistic100million.png" align="center">
  <figcaption style="font-size: 75%;">
  Histogram of stopping times for the numbers 1 to 100 million.
  <br>
  Stopping time is on the \(x\) axis, frequency on the \(y\) axis. Source: 
  Wikipedia
  </figcaption>
</figure>
Mathematically, this problem is quite easy to solve using Python, although 
it runs a bit slow for my preferences. First, we code a function that 
returns the next number in the Collatz sequence. 
<pre><code>
"""
def increment_collatz(n):
  return (n / 2) if (n % 2 == 0) else (3 * n + 1)
"""
</code></pre>
<p>
In order to improve the efficiency of our algorithm, it's important to notice
that we don't need to calculate the entire Collatz sequence of every number. 
Since the calculation of a Collatz sequence is deterministic, we can cache
previously calculated sequence lengths to assist us. That is, if we increment
out starting values when trying to find the length of the Collatz sequence 
starting with <tt>start value = 13</tt> (as per the example above), we only 
need to determine the next three terms (40, 20, 10). Since we will have 
determined the length of the Collatz sequence for 10 by the time we've moved
on to 13, we can stop calculating Collatz values as long as we've cached the
length corresponding to the sequence for 10. Unfortunately, we can't cache 
the length of every value that gets computed, as could possibly tax the system
memory too much (for example, running 32 bit Python 2.7.10 on a computer with
4 GB of RAM encountered memory errors when trying to cache the length of all 
computed Collatz sequences). This is due to the fact that many terms of a given 
Collatz sequence will be higher than their initial start value. To this end,
we only cache the sequence lengths for values less than 1,000,000. 
</p>
<pre><code>
"""
import time
def euler14():
  ubound = 1000000

  cache = [0] * ubound
  cache[0] = 'undefined'
  cache[1] = 1
  max_length = 1
  
  for start_value in range(2, ubound):
    current_collatz = start_value
    current_length = 1

    while True:
      current_collatz = increment_collatz(current_collatz)
      if current_collatz < start_value:
        current_length += cache[current_collatz]
        break
      else:
        current_length += 1

    cache[start_value] = current_length
    if current_length > max_length:
      max_length = current_length
      solution = start_value
  
  return (solution, max_length)
        
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d \nChain length: %d' % euler14())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 837799
Chain length: 525
Time elapsed: 2.020999
</pre>

<p>
While I was hoping for a solution that ran in less than a second, most Python solutions
to this problem I've found online also run at about this speed, although it is possible
to get sub one second results using lower-level languages.
</p>
"""
