#!/usr/local/bin/python2.7

import time
from itertools import product

def euler31(total):
  """
  How many different ways can two British pounds (200p) be made using
  any number of coins, where the coins are: 1, 2p, 5p, 10p, 20p, 50p, 
  100p, and 200p.
  """
  
  denominations = [200, 100, 50, 20, 10, 5, 2, 1]
  quotients = [total / denomination for denomination in denominations]
  quotients = [q + 1 for q in quotients]
  
  runningTotal = 0
  for permutation in product(*map(range, quotients)):
    print permutation
    #if sum([count * denomination for (count, denomination) in zip(permutation, denominations)]) == total:
    #  runningTotal += 1

  return runningTotal

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler31(200))
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
