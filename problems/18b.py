<p>
<strong>Statement: </strong>By starting at the top of the triangle 
below and moving to adjacent numbers on the row below, the maximum 
total from top to bottom is 23.
</p>
<pre style="text-align:center;">
<b>3</b>
<b>7</b> 4
2 <b>4</b> 6
8 5 <b>9</b> 3
</pre>
<p>
That is, 3 + 7 + 4 + 9 = 23. Find the maximum total from top to bottom 
of the triangle below:
</p>
<pre style="text-align: center;">
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
</pre>
<hr />
<p>
While one could feasibly brute-force this by checking every possibly
path (as there are only 16,384 possible routes), we will instead turn
this question on its head (literally) to get a much more efficient 
solution. 
</p>
<p>
For demonstrative purposes, let's consider the example triangle they give:
</p>
<pre style="text-align:center;">
3
7 4
2 4 6
8 5 9 3
</pre>
<p>
Notice that once we find the correct path, it doesn't matter if we traverse
that path from top to bottom or bottom to top; we get the same sum either 
way. With this in mind, I will actually construct the path with the maximum
sum from bottom to top.
</p>
<p>
Let's examine the third row (or more generally, the 
second-to-last row), and its first element (2). From this spot, we can 
either move down to 8 or to 5. Since 8 is the larger number of my two 
options (leading to the larger path sum), I'm going to replace the 2 by 
the sum 2 + 8 = 10. Moving on to the next element in row three (4), I see 
that of the two options (5 and 9), moving to 9 next would give me 
the larger sum. So I replace 4 with 4 + 9 = 13. Continuing on like this,
I get a new triangle:
</p>
<pre style="text-align:center;">
3
7  4
10 13 15
</pre>
<p>
Let's now use this same approach on the second row. Replacing each
entry of row two with the larger of the two possible paths, we replace
7 with 7 + 13 = 20 and replace 4 with 4 + 15 = 19:
</p>
<pre style="text-align:center;">
3
20  19
</pre>
<p>
Finally, replacing 3 with the larger of the two possible paths (3 + 30 = 23)
gives us our solution. 
</p>
<p>
This is the approach our code will take. Notice that due to how the array 
will be set up, that going "down and to the left" will be simply increasing 
the row number and staying in the same column, while moving "down and to 
the right" will be increasing both the row and column numbers. We start
with the second to last row (13, using 0-index arrays) and traverse 
backwards to get our solution, which will be stored as the 0th index in the
0th list in our arra.
</p>
<pre><code>
import time

def euler():

  """
  Create the triangle in an array
  """
  
  triangle = [
              [75],
              [95, 64],
              [17, 47, 82],
              [18, 35, 87, 10],
              [20,  4, 82, 47, 65],
              [19,  1, 23, 75,  3, 34],
              [88,  2, 77, 73,  7, 63, 67],
              [99, 65,  4, 28,  6, 16, 70, 92],
              [41, 41, 26, 56, 83, 40, 80, 70, 33],
              [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
              [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
              [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
              [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
              [63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
              [ 4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23]
             ]
  #triangle = list(reversed(triangle))

  """
  Go through each row, starting on the second to last. In this row,
  replace each entry with the sum of itself and the maximum of the 
  two possible values below it. The largest sum will be stored in the
  top value, triangle[0][0].
  """ 
  
  for i in range(13, -1, -1):
    row = triangle[i]
    for j in range(len(row)):
      triangle[i][j] += max(triangle[i + 1][j], triangle[i + 1][j + 1])
  
  return triangle[0][0] 
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
</code></pre>
<hr>
<pre>
The solution is: 1074
Time elapsed: 0.001000
</pre>
