#!/usr/local/bin/python2.7

import time
import itertools

def euler():
  """
  The sums traveling from top to bottom will be the same as the sums 
  travelling from bottom to top. Start at the second to bottom row, 
  and for each number  in that row, do the following: Take the maximum 
  of the numbers directly down and left or down and right from , and add 
  that to . Now do the same for the third-to-last row, then fourth-to-last,
  and so on. We’re modifying the triangle as we go to produce maximum 
  partial sums from the bottom, and the last stage will be to replace 
  to top number in the triangle, which will then be the maximum sum.
  """
  triangle = []
  line = '75'
  triangle.append(map(int, line.split()))
  line = '95 64'
  triangle.append(map(int, line.split()))
  line = '17 47 82'
  triangle.append(map(int, line.split()))
  line = '18 35 87 10'
  triangle.append(map(int, line.split()))
  line = '20 04 82 47 65'
  triangle.append(map(int, line.split()))
  line = '19 01 23 75 03 34'
  triangle.append(map(int, line.split()))
  line = '88 02 77 73 07 63 67'
  triangle.append(map(int, line.split()))
  line = '99 65 04 28 06 16 70 92'
  triangle.append(map(int, line.split()))
  line = '41 41 26 56 83 40 80 70 33'
  triangle.append(map(int, line.split()))
  line = '41 48 72 33 47 32 37 16 94 29'
  triangle.append(map(int, line.split()))
  line = '53 71 44 65 25 43 91 52 97 51 14'
  triangle.append(map(int, line.split()))
  line = '70 11 33 28 77 73 17 78 39 68 17 57'
  triangle.append(map(int, line.split()))
  line = '91 71 52 38 17 14 91 43 58 50 27 29 48'
  triangle.append(map(int, line.split()))
  line = '63 66 04 68 89 53 67 30 73 16 69 87 40 31'
  triangle.append(map(int, line.split()))
  line = '04 62 98 27 23 09 70 98 73 93 38 53 60 04 23'
  triangle.append(map(int, line.split()))
  
  for i in range(-2, -len(triangle), -1):
    for j in range(len(triangle[i])):
      triangle[i][j] += max(triangle[i + 1][j], triangle[i + 1][j + 1])
  
  return triangle[0][0] + max(triangle[1][0], triangle[1][1])
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 137846528820
Time elapsed: 0.012000
</pre>
"""