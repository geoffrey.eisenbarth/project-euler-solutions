#!/usr/local/bin/python2.7

import time
from string import ascii_uppercase

def name_score(name):
  # offset the index by 1, since ascii_uppercase.index('A') == 0 
  name_digits = [ascii_uppercase.index(letter) + 1 for letter in name]
  return sum(name_digits)
  
def euler():
  """
  Using names.txt, a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical 
  order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in
  the list to obtain a name score.

  For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is
  the 938th name in the list. So, COLIN would obtain a score of 938 x 53 = 49714.
  
  What is the total of all the name scores in the file?
  """
  with open('names.txt') as file:
    names = file.read()               # read all the names
    names = names.replace('"', '')    # remove quotation marks
    names = nams.split(',')           # split on comma into a list
    names = sorted(names)             # sort the list alphabetically 
  
  # offset index by 1, since names is 0-based indexed. Return the solution
  return sum([name_score(name) * (names.index(name) + 1) for name in names])

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
</code></pre>
<hr>
<pre>
The solution is: 648
Time elapsed: 0.000999
</pre>