#!/usr/local/bin/python2.7

import time
from math import log10
from math import sqrt
from itertools import product
from itertools import permutations

"""
Too slow to go through all permutations, but interesting approach
"""
def euler32_slow():
  """
  We shall say that an n-digit number is pandigital if it makes use of all the digits 
  1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

  The product 7254 is unusual, as the identity, 39 * 186 = 7254, containing multiplicand,
  multiplier, and product is 1 through 9 pandigital.

  Find the sum of all products whose multiplicand/multiplier/product identity can be 
  written as a 1 through 9 pandigital.

  HINT: Some products can be obtained in more than one way so be sure to only include it
  once in your sum.
  """
  pandigital = []
  digits = '123456789'
  for permutation in permutations(digits):
    permutation = ''.join(permutation)          #convert from tuple to string
    """
    If the product ends in five, then since zero isn't include,
    so must one of the multiplicands. This results in double 5.
    """
    if permutation[-1] == '5': continue 
      
    #left and right represent partition spots.
    for left in range(1, 9):
      if permutation[left - 1] == '5': break    #see above
              
      for right in range(left + 1, 9):
        if permutation[right - 1] == '5': break #see above
        
        """
        The number of digits in the product is given by the left hand side of
        the first and statement below. We utilize Python's short-circuiting of
        logic statements for efficiency. 
        """         
        a, b = int(permutation[:left]), int(permutation[left:right])
        if int(log10(a) + log10(b) + 1) == 9 - right and a * b == int(permutation[right:]):  
          pandigital.append(int(permutation[right:]))
  return sum(set(pandigital))

"""
More efficient approach
"""

def isPandigitalString(string):
  for i in xrange(1, 10):
    if str(i) not in string:
      return False
  return True
  
def givesPandigitalProduct(a, b):
  numbers = str(a) + str(b) + str(a * b)
  return isPandigitalString(numbers)
 
def euler32():
  pandigitals = []
  for a in xrange(1, int(sqrt(9876543))):
    for b in xrange(a, int(sqrt(9876543))):
      if len(str(a) + str(b) + str(a * b)) > 9:
        break
      if givesPandigitalProduct(a, b):
        pandigitals.append(a * b)
        print('%d * %d = %d' % (a, b, a * b))
  return sum(set(pandigitals))
 
  
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler32())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
