#!/usr/local/bin/python2.7

import time
import itertools
import sys
import math

def euler25():
  """
  What is the first term in the Fibonacci sequence to contain 1000 digits?
  """
  
  fibonacci = [1, 1]
  i = 1  
  while True:
    num = fibonacci[-1] + fibonacci[-2]
    fibonacci.append(num)
    if int(math.log10(num)) + 1 > 1000:
      return num
    else:
      i += 1
      
  #return [fibonacci(num) for num in range(1, 1000) if int(math.log10(fibonacci(num))) + 1 > 1000]
  #return [fibonacci(num) for num in range(1, 1000) if len(str(fibonacci(num))) > 1000]
  
#boilerplate code to run
def main():
  start = time.time()
  print 'The answer is %d' % euler25()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()