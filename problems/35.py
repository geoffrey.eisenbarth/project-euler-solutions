#!/usr/local/bin/python2.7

import time
import math
import itertools

def prime_list(ubound):
  """
  Returns a list of Booleans indicating whether their index is prime. 
  """
  #initialize the sieve
  primes = [2, 3]
  sieve = [False] * (ubound + 1)
  
  #put in candidate primes:
  #integers which have an odd number of
  #representations by certain quadratic forms
  for x, y in itertools.product(range(1, int(math.sqrt(ubound)) + 1), range(1, int(math.sqrt(ubound)) + 1)):
    n = 4 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 1 or n % 12 == 5):
      sieve[n] = not sieve[n]
      
    n = 3 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 7):
      sieve[n] = not sieve[n]
      
    n = 3 * x ** 2 - y ** 2
    if (x > y) and (n <= ubound) and (n % 12 == 11):
      sieve[n] = not sieve[n]
      
  #eliminate composites by sieving
  for n in xrange(5, int(math.sqrt(ubound)) + 1):
    if sieve[n]:
      #n is prime, omit multiples of its square; this is
      #sufficient because composites which managed to get
      #on the list cannot be square-free
      for k in xrange(n ** 2, ubound, n ** 2):
        sieve[k] = False
        
  #list doesn't account for 2 and 3
  sieve[2], sieve[3] = True, True
  
  return sieve
  
def rotate_int(x, i):
  return int(str(x)[i:] + str(x)[:i])
  
def euler35():
  """
  The number, 197, is called a circular prime because all rotations 
  of the digits: 197, 971, and 719, are themselves prime.

  There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 
  31, 37, 71, 73, 79, and 97.

  How many circular primes are there below one million?
  """

  """
  Brute Force:
  
  count = 4       #account for 2, 3, 5, 7
  for candidate in xrange(11, 1000000, 2):
    i = 0
    rotation = candidate
    while is_prime(rotation):
      i += 1
      rotation = int(str(candidate)[i:] + str(candidate)[:i])
      if i > len(str(candidate)):
          count += 1
          break
          
  return count
  """
  """
  Sieve
  """
  
  is_prime = prime_list(1000000)
  list_of_primes = [n for n, e in enumerate(is_prime) if e]
  
  count = 0
  for candidate in list_of_primes:
    i = 1
    rotation = rotate_int(candidate, i) 
    while is_prime[rotation]:
      i += 1
      rotation = rotate_int(candidate, i) 
      if i > len(str(candidate)):
          count += 1
          break
  return count
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler35())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
