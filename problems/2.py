#!/usr/local/bin/python2.7
import time
from math import floor 

def fibonacci1(n):
  if n == 0 or n == 1:
    return n
  return fibonacci(n-1) + fibonacci(n-2)
  
def fibonacci2(n):
  phi = (1 + 5 ** 0.5) / 2
  return int(floor((phi ** n) / (5 ** 0.5) + 0.5))
  
def euler2():
  """
  Solution to Euler2: Find the sum of of the even terms in the Fibonacci 
  sequence that do not exceed 4000000
  """
  running_sum = 0
  for i in range(3, 34):
    running_sum += fibonacci2(i)
    i += 3
  return running_sum
      
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler2())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
