#!/usr/local/bin/python2.7

import time
from math import sqrt

def is_prime(n):

  #special cases 2 and 3, excludes (-infty, 1]
  if n <= 3:
    return n >= 2  
  
  #Only need to check for divisors less than the square-root
  ubound = int(sqrt(n)) + 1
  
  #Since all primes except 2 and 3 are of the form p = 6k +/- 1, 
  #we look for prime divisors of this form. Note, that just because 
  #a number has the form 6k +/- 1 does not mean it will be prime, but
  #we are sure to pick up all primes less than ubound with this
  #approach. 
  
  if n % 2 == 0 or n % 3 == 0 or n % 5 == 0:
    return False
   
  k = 1
  while 6 * k + 1 <= ubound:
    if n % (6 * k + 1) == 0 or n % (6 * k - 1) == 0:
      return False
    k += 1
  
  return True
  
def euler46():
  """"
  It was proposed by Christian Goldbach that every odd composite number can be 
  written as the sum of a prime and twice a square.

   9 = 7  + 2 * (1 ** 2)
  15 = 7  + 2 * (2 ** 2)
  21 = 3  + 2 * (3 ** 2)
  25 = 7  + 2 * (3 ** 2)
  27 = 19 + 2 * (2 ** 2)
  33 = 31 + 2 * (1 ** 2)

  It turns out that the conjecture was false.

  What is the smallest odd composite that cannot be written as the sum of a 
  prime and twice a square?
  """
  candidates = []
  candidate = 35
  while True:
    if not is_prime(candidate):
      #odd = 2 * k + 1
      k = candidate // 2 #integer division
      
      tally = 0
      for possible_square in range(1, k)[::-1]:
        if not sqrt(possible_square).is_integer() or not is_prime(candidate - 2 * possible_square):
          tally += 1
      if tally == len(range(1, k)):
        return candidate
   
    candidate += 2

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler46())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
