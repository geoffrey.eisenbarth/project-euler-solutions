#!/usr/local/bin/python2.7

import time

def is_palindromic(number, base):
  if base == 10:
    number = str(number)
  elif base == 2:
    number = str(bin(number))[2:]
  else:
    return None
  return number == number[::-1]
    
def euler36():
  """
  The decimal number, 585 = 1001001001 (binary), is palindromic in 
  both bases.

  Find the sum of all numbers, less than one million, which are 
  palindromic in base 10 and base 2.

  (Please note that the palindromic number, in either base, may not 
  include leading zeros.)
  """
  running_total = 0
  for candidate in xrange(1000000):
    if is_palindromic(candidate, 10) and is_palindromic(candidate, 2):
      running_total += candidate
  
  return running_total    
    
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler36())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
