#!/usr/local/bin/python2.7

import time
import itertools

def euler17():
  
  """
  We create a dictonary to allow us to convert from
  integers to words. We manually key in the core words
  """
  int_to_word = { 
                  1    : 'one',
                  2    : 'two', 
                  3    : 'three',
                  4    : 'four',
                  5    : 'five', 
                  6    : 'six',
                  7    : 'seven',
                  8    : 'eight',
                  9    : 'nine', 
                  10   : 'ten',
                  11   : 'eleven',
                  12   : 'twelve',
                  13   : 'thirteen',
                  14   : 'fourteen',
                  15   : 'fifteen',
                  16   : 'sixteen',
                  17   : 'seventeen',
                  18   : 'eighteen',
                  19   : 'nineteen',
                  20   : 'twenty', 
                  30   : 'thirty',
                  40   : 'forty',
                  50   : 'fifty',
                  60   : 'sixty',
                  70   : 'seventy',
                  80   : 'eighty',
                  90   : 'ninety'
                 }

  
  #Fill in the tens
  for i, j in itertools.product(range(2, 10), range(1, 10)):
    int_to_word[int(str(i) + str(j))] = int_to_word[i * 10] + ' ' + int_to_word[j]
    
  #Fill in the hundos
  for k in range(1, 10):
    int_to_word[k * 100] = int_to_word[k] + ' hundred'
  
    for i in range(1, 20):
      int_to_word[int(str(k * 100 + i))] = int_to_word[k * 100] + ' and ' + int_to_word[int(str(i))]
     
    for i, j in itertools.product(range(2, 10), range(0, 10)):
      int_to_word[int(str(k * 100 + i * 10 + j))] = int_to_word[k * 100] + ' and ' + int_to_word[int(str(i * 10 + j))]
    
  #Fill in 1000
  int_to_word[1000] = 'one thousand'
  
  return len(''.join(int_to_word.values()).replace(' ', ''))
  
#boilerplate code to run
def main():
  start = time.time()
  print euler17()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()