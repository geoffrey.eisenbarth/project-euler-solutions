#!/usr/local/bin/python2.7

import time
import string

def euler22():
  """
  Using names.txt, a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical 
  order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in
  the list to obtain a name score.

  For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is
  the 938th name in the list. So, COLIN would obtain a score of 938 x 53 = 49714.
  
  What is the total of all the name scores in the file?
  """
  file = open('names.txt')
  names = sorted(file.read().replace('"', '').split(','))
  file.close()
  
  return sum([sum([(string.ascii_uppercase.index(char) + 1) for char in name]) * (names.index(name) + 1) for name in names])

#boilerplate code to run
def main():
  start = time.time()
  print 'The answer is %d' % euler22()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()