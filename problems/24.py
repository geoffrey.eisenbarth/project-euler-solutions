#!/usr/local/bin/python2.7

import time
import itertools

def euler24():
  """
  What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
  """
  #itertools.permutations returns a tuple in lexicographic order as long as
  #the original string increases numerically
  for i, tuple in enumerate(itertools.permutations('0123456789', 10)):
    if i == 999999:
      return int("".join(tuple))
 
#boilerplate code to run
def main():
  start = time.time()
  print 'The answer is %d' % euler24()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()