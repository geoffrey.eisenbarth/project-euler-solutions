""""
<p>
<strong>Statement: </strong> Let \(d(n)\) be defined as the sum of proper divisors of \(n\) (numbers less than n which divide evenly into \(n\)). If \(d(a) = b\) and \(d(b) = a\), where \(a \neq b\), then \(a\) and \(b\) are an amicable pair and each of \(a\) and \(b\) are called amicable numbers.

For example, the proper divisors of \(220\) are 
\[1, 2, 4, 5, 10, 11, 20, 22, 44, 55, \mbox{and} 110;\]
therefore \(d(220) = 284\). The proper divisors of \(284\) are 
\[1, 2, 4, 71, \mbox{and} 142;\] 
so \(d(284) = 220\).

Evaluate the sum of all the amicable numbers under \(10000\).
</p>
<hr />
"""
#!/usr/local/bin/python2.7

import time
import sys
from itertools import count, islice
from math import sqrt

def is_prime(n):
  if n < 2: return False
  return all(n % i for i in islice(count(2), int(sqrt(n) - 1)))
  
def proper_divisors(n):
  # get pairs of all divisors except 1 and n
  pairs = [(i, n / i) for i in islice(count(2), int(sqrt(n) - 1)) if n % i == 0]
  
  # flatten to list
  divisors = list(sum(pairs, ())
  
  #add 1 and remove duplication if n is a sqrt
  return [1] + list(set(divisors))

def euler21(upper_bound):
  """
  Let d(n) be defined as the sum of proper divisors of n (numbers less than n 
  which divide evenly into n). If d(a) = b and d(b) = a, where a != b, then a 
  and b are an amicable pair and each of a and b are called amicable numbers.

  For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 
  55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 
  71 and 142; so d(284) = 220.

  Evaluate the sum of all the amicable numbers under 10000.
  """
  
  # Brute Force, utilizing that all KNOWN amicable pairs are 1) not prime, 
  # 2) both even or both odd, and 3) share a common factor greater than 1. 
  # (Note: 2 and 3 haven't been proved for all amicable numbers yet)
  
  tally = 0 
  for i in xrange(2, 10000):
    if not is_prime(i):
      sum1 = sum(prime.proper_divisor_list(i))
      
      if sum1 > 1 and not prime.is_prime(sum1):
        sum2 = sum(prime.proper_divisor_list(sum1))
        if sum2 == i and sum1 != sum2:
          tally += sum1 + sum2
  return tally

# boilerplate code to run
def main():
  upper_bound = int(raw_input('Insert upper bound: '))
  start = time.time()
  print 'The sum of all the amicable numbers under %d is %d' % (upper_bound, euler21(upper_bound))
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()