#!/usr/local/bin/python2.7

import time

def euler4():
  """
  Solution to Euler4: Find the largest palindrome made from the product of two 3-digit numbers.
  """
  
  candidate = 0
  for i in xrange(999,100,-1):
    for j in xrange(999, 100, -1):
      
      if i * j > candidate:
        if str(i * j) == str(i * j)[::-1]:
          candidate = i * j       
  return candidate
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler4())
  elapsed = (time.time() - start)
  print('Time elapsed: %s seconds' % str(elapsed))

if __name__ == '__main__':
  main()
