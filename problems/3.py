#!/usr/local/bin/python2.7

import time

def euler3():
  """
  Solution to Euler3: What is the largest prime factor of the number 600851475143?
  Method: Check for factors up to the square root of the ubound
  """
 
  factor = 600851475143
  i = 2  
  while i * i < factor:
    while factor % i == 0:
      factor = factor / i
    i += 1
  return factor   
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler3())
  elapsed = (time.time() - start)
  print('Time elapsed: %s seconds' % str(elapsed))

if __name__ == '__main__':
  main()
