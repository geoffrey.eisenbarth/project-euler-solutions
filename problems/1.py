import time

def sum_of_multiples(n):
  return n * (1000 // n) * (1000 // n + 1) / 2

def euler1():
  return sum_of_multiples(3) + sum_of_multiples(5) - 3 * 5
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler1())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()