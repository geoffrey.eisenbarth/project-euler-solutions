#!/usr/local/bin/python2.7

import time
import itertools
from math import sqrt
from collections import defaultdict

def prime_list(ubound):
  """
  Returns a list of primes utilizing the Sieve of Atkin
  """
  #initialize the sieve
  primes = [2, 3]
  sieve = [False] * (ubound + 1)
  
  #put in candidate primes:
  #integers which have an odd number of
  #representations by certain quadratic forms
  for x, y in itertools.product(range(1, int(sqrt(ubound)) + 1), range(1, int(sqrt(ubound)) + 1)):
    n = 4 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 1 or n % 12 == 5):
      sieve[n] = not sieve[n]
    
    n = 3 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 7):
      sieve[n] = not sieve[n]
      
    n = 3 * x ** 2 - y ** 2
    if (x > y) and (n <= ubound) and (n % 12 == 11):
      sieve[n] = not sieve[n]
      
  #eliminate composites by sieving
  for n in xrange(5, int(sqrt(ubound)) + 1):
    if sieve[n]:
      #n is prime, omit multiples of its square; this is
      #sufficient because composites which managed to get
      #on the list cannot be square-free
      for k in xrange(n ** 2, ubound, n ** 2):
        sieve[k] = False
  
  #list doesn't account for 2 and 3
  return [2, 3] + [n for n, e in enumerate(sieve) if e]
  
def euler50():
  """
  The prime 41, can be written as the sum of six consecutive primes:

  41 = 2 + 3 + 5 + 7 + 11 + 13
  This is the longest sum of consecutive primes that adds to a prime below one-hundred.

  The longest sum of consecutive primes below one-thousand that adds to a prime, contains 
  21 terms, and is equal to 953.

  Which prime, below one-million, can be written as the sum of the most consecutive primes?

  """
  candidates = prime_list(1000000):
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler50())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
