#!/usr/local/bin/python2.7

import time
from fractions import Fraction

"""
Too slow to go through all permutations, but interesting approach
"""
def euler33():
  """
  The fraction 49/98 is a curious fraction, as an inexperienced mathematician in 
  attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is 
  correct, is obtained by cancelling the 9s.

  We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

  There are exactly four non-trivial examples of this type of fraction, less than 
  one in value, and containing two digits in the numerator and denominator.

  If the product of these four fractions is given in its lowest common terms, find 
  the value of the denominator.
  """
  running_product = Fraction(1, 1)
  
  for numerator in range(10, 100):
    for denominator in range(numerator + 1, 100):
    
      numerator, denominator = str(numerator), str(denominator)
      
      for digit in numerator:
        if digit in denominator and digit != '0':
          
          try:      
            fraction_prime  = Fraction(int(numerator.replace(digit, '', 1)),
                                       int(denominator.replace(digit, '', 1)))
            fraction        = Fraction(int(numerator), 
                                       int(denominator))
                                       
            if fraction_prime == fraction:
              running_product *= fraction
              
          except: 
            """
            It's possible for denominator to be zero, so we catch that error here
            """
            continue
            
  return running_product.denominator
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler33())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
