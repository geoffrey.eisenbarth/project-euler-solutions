"""
<p>
<strong>Statement: </strong>   You are given the following information, 
but you may prefer to do some research for yourself.
<p>
<ul>
  <li>1 Jan 1900 was a Monday.</li>
  <li>Thirty days has September,
      April, June and November.
      All the rest have thirty-one,
      Saving February alone,
      Which has twenty-eight, rain or shine.
      And on leap years, twenty-nine.</li>
   <li>A leap year occurs on any year evenly divisible by 4, but not on 
       a century unless it is divisible by 400.</li>
</ul>
How many Sundays fell on the first of the month during the twentieth 
century (1 Jan 1901 to 31 Dec 2000)?
</p>
<hr />
<p>
I'm going to attack this one in a kind of boring manner: brute-force. The 
first thing I want to do is craft a function that returns the number of 
days in a given month (with the year specified). Using the rhyme above, 
this is pretty straight-forward, although the logic might look a little
convoluted (a "select case" Python statement would be nice here).
</p>
<pre><code>
"""
def days_in_month(month, year):
  if month in [4, 6, 9, 11]: 
    return 30
  elif month == 2:
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0): 
      return 29
    else:
      return 28  
  else:
    return 31
"""
</code></pre>
<p>
Our script will loop through all the years from 1900 to 2000 (inclusive) 
and each month adding up the number of days. We will keep track of how 
many days have passed since January 1st 1900 in the variable 
<tt>days_since_1_jan_1900</tt>. We will be able to tell if the first of
the month is a Sunday precisely when this counter is divisible by seven.
In the case that it is, we increment <tt>sundays_counter</tt> and move on.
Notice because the question asks about the twentieth century (starting
1 January 1901), but our information starts on 1 January 1900, we have
an extra bit of logic in the <tt>if</tt> statement.
</p>
<pre><code>
"""
import time

def euler():
  sundays_counter = 0
  days_since_1_jan_1900 = 1
  for year in xrange(1900, 2001):
    for month in xrange(1, 13):
      if year != 1900 and days_since_1_jan_1900 % 7 == 0:
        sundays_counter += 1
      days_since_1_jan_1900 += days_in_month(month, year)
      
  return sundays_counter
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 171
Time elapsed: 0.000999
</pre>
"""