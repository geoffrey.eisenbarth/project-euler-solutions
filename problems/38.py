#!/usr/local/bin/python2.7

import time
import itertools

def concatenated_product(number, ubound):
  product = ''
  for i in range(1, ubound + 1):
    product += str(number * i)
  return int(product)

def is_pandigital(number):
  digits = set(['1', '2', '3', '4', '5', '6', '7', '8', '9'])
  if len(str(number)) == 9 and set([digit for digit in str(number)]) == digits:
    return True
  return False
  
def euler38():
  """
  Take the number 192 and multiply it by each of 1, 2, and 3:

  192 * 1 = 192
  192 * 2 = 384
  192 * 3 = 576
  By concatenating each product we get the 1 to 9 pandigital, 
  192384576. We will call 192384576 the concatenated product of 
  192 and (1,2,3)

  The same can be achieved by starting with 9 and multiplying by 
  1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the 
  concatenated product of 9 and (1,2,3,4,5).

  What is the largest 1 to 9 pandigital 9-digit number that can be
  formed as the concatenated product of an integer with (1,2, ... , n)
  where n > 1?
  """
  candidates = []
  
  for number in range(1, 10000):
    if number < 10:
      ubound = 5
    elif number < 100:
      ubound = 4
    elif number < 1000:
      ubound = 3
    else:
      ubound = 2
      
    candidate = concatenated_product(number, ubound)
    if is_pandigital(candidate):
      candidates.append((number, ubound, candidate))
    
  return sorted(candidates, key = lambda x: x[2])[-1][2]  
    
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler38())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
