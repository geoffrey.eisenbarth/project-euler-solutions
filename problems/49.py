#!/usr/local/bin/python2.7

import time
import itertools
from math import sqrt
from collections import defaultdict

def prime_list(ubound):
  """
  Returns a list of primes utilizing the Sieve of Atkin
  """
  #initialize the sieve
  primes = [2, 3]
  sieve = [False] * (ubound + 1)
  
  #put in candidate primes:
  #integers which have an odd number of
  #representations by certain quadratic forms
  for x, y in itertools.product(range(1, int(sqrt(ubound)) + 1), range(1, int(sqrt(ubound)) + 1)):
    n = 4 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 1 or n % 12 == 5):
      sieve[n] = not sieve[n]
    
    n = 3 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 7):
      sieve[n] = not sieve[n]
      
    n = 3 * x ** 2 - y ** 2
    if (x > y) and (n <= ubound) and (n % 12 == 11):
      sieve[n] = not sieve[n]
      
  #eliminate composites by sieving
  for n in xrange(5, int(sqrt(ubound)) + 1):
    if sieve[n]:
      #n is prime, omit multiples of its square; this is
      #sufficient because composites which managed to get
      #on the list cannot be square-free
      for k in xrange(n ** 2, ubound, n ** 2):
        sieve[k] = False
  
  #list doesn't account for 2 and 3
  return [2, 3] + [n for n, e in enumerate(sieve) if e]
  
def is_permutation(num1, num2):
  if len(str(num1)) == len(str(num2)):
    return sorted(str(num1)) == sorted(str(num2))
  return False

def is_arithmetic(sequence):
  return len(set([sequence[i+1] - sequence[i] for i in range(len(sequence) - 1)])) == 1
  
def euler49():
  """
  The arithmetic sequence, 1487, 4817, 8147, in which each of the terms 
  increases by 3330, is unusual in two ways: (i) each of the three terms 
  are prime, and, (ii) each of the 4-digit numbers are permutations of 
  one another.

  There are no arithmetic sequences made up of three 1-, 2-, or 3-digit 
  primes, exhibiting this property, but there is one other 4-digit 
  increasing sequence.

  What 12-digit number do you form by concatenating the three terms in 
  this sequence?
  """
  
  prime_range = [prime for prime in prime_list(9999) if prime > 1000]
  for prime1 in prime_range:
    candidate = [prime1]
    for prime2 in [prime for prime in prime_range if prime > prime1]:
      if is_permutation(prime1, prime2):
        candidate.append(prime2)
    if len(candidate) == 3 and is_arithmetic(candidate):
      return int(str(candidate[0]) + str(candidate[1]) + str(candidate[2]))
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler49())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
