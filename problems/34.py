#!/usr/local/bin/python2.7

import time
from math import factorial

def euler34():
  """
  145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

  Find the sum of all numbers which are equal to the sum of the factorial of their digits.

  Note: as 1! = 1 and 2! = 2 are not sums they are not included.
  """
  
  """
  Find upper bound on number of digits:
  
  Find M such that for all m >= M, we have
  M * 9! > 9 * 10 ** (M-1) + 9 * 10 ** (M-2) + ... + 9 * 10 ** 0.
  Divide both sides by 9 and solve for M by graphing. It
  turns out that M = 5 will do. 
  
  """
  curious_numbers = []
  for candidate in xrange(10, 99999):
    if sum([factorial(int(digit)) for digit in str(candidate)]) == candidate:
      curious_numbers.append(candidate)
  return sum(curious_numbers)
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler34())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()
