#!/usr/local/bin/python2.7

import sys
import time
import math

def gcd(a, b):
  if b == 0:
    return a
  else:
    return gcd(b, a % b)

def lcd(a, b):
  return (abs(a) / gcd(a, b) ) * abs(b)

def is_prime(n):

  #special cases 2 and 3, excludes (-infty, 1]
  if n <= 3:
    return n >= 2  
  
  #Only need to check for divisors less than the square-root
  ubound = int(math.sqrt(n)) + 1
  
  #Since all primes except 2 and 3 are of the form p = 6k +/- 1, 
  #we look for prime divisors of this form. Note, that just because 
  #a number has the form 6k +/- 1 does not mean it will be prime, but
  #we are sure to pick up all primes less than ubound with this
  #approach. 
  
  if n % 2 == 0 or n % 3 == 0:
    return False
   
  k = 1
  while 6 * k + 1 <= ubound:
    if n % (6 * k + 1) == 0 or n % (6 * k - 1) == 0:
      return False
    k += 1
  
  return True

def prime_factorization(number):
  """
  Not efficient, only good for small numbers.
  
  Returns a list of tuples (prime factor, exponent)
  """
  factor = 2
  factorization = []
  while factor <= number:
    if is_prime(factor) and number % factor == 0:
      exponent = 0
      while number % (factor ** (exponent + 1)) == 0:
        exponent += 1
      factorization.append((factor, exponent))
    factor += 1
  
  return factorization
    
def euler5():
  factors = []
  for number in range(1, 21):
    factors += prime_factorization(number)
  
  """
  Discard duplicates
  """
  factors = [tuple for tuple in set(factors)] 
  
  """
  single out distinct factors with highest exponent 
  (which are at end of list)
  """
  for tuple in factors[::-1]:
    for exponent in range(tuple[1]):
      if (tuple[0], exponent) in factors:
        factors.remove((tuple[0], exponent))
  
  """
  turn factors into a list of factors and return their product
  """
  factors = [prime ** exponent for (prime, exponent) in factors]
  return reduce(lambda x, y: x * y, factors)
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler5())
  elapsed = (time.time() - start)
  print('Time elapsed: %s seconds' % str(elapsed))

if __name__ == '__main__':
  main()

