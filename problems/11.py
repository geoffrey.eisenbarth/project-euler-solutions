
<p>
<strong>Statement: </strong>In the 20 by 20 grid below, the product of 
four numbers in a diagonal is \(26 \times 63 \times 78 \times 14 = 1788696.\) 
</p>
<pre>
08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48
</pre>
<p>
What is the greatest product of four adjacent numbers in the 
same direction (up, down, left, right, or diagonally) in the 
20 by 20 grid?
</p>
<hr>
This problem doesn't have any particularly interesting mathematics
in my opinion, so I wanted to take this chance to talk about one 
of my favorite features of Python: lambda functions. These functions, 
often called <em>nnonymous functions</em> are not bound to any given 
name, but rather serve to produce quick functionality for situations 
where one needs to implement a function call only a few times. 
</p>
<p>
For instance, since we will be trying to determine the product of
four adjacent numbers in our matrix (and since Python doesn't have
a built in <kbd>product()<kbd> function), I will create a function <kbd>slice_product()</kbd>
that will return the product of all entries in a list. To do this, I will utilize
the built in <kbd>reduce()</kbd> function. <kbd>reduce()</kbd> takes two arguments,
a function of two variables and an iterable, and returns the value obtained by 
cumulatively applying the function to the items of iterable. This is done from
left to right, and results in a single value. The syntax and result are demonstrated
as follows:
</p>
<pre style="text-align:center;">
reduce(f(x, y), [1,2,3, 4]) -> f(f(f(1, 2), 3), 4)
</pre>
<p>
Since Python doesn't have a built in <kbd>product()</kbd> function, we will have to 
create one ourselves. We could create a named function as such:
</p>
<pre><code>
def product(x, y):
  return x * y
</code></pre>
<p>
However, the only time this function will be referenced is during the function 
<kbd>slice_product()</kbd>. To this end I will use a lambda function instead, defining
<kbd>slice_product()</kbd> as follows: 
</p>
<pre><code>

def slice_product(slice):
  return reduce(lambda x, y : x * y, slice)

</code></pre>
<p>
This takes a slice, and cumulatively applies the annonymous product function to sucessive
terms of the input list. The syntax for lambda functions is 
<kbd>lambda x1, x2, ..., xn : &lt;function definition of n variables&gt;</kbd>.
</p>
<p>
After <kbd>slice_product()</kbd> has been defined, the rest of the code is straight-forward.
</p>
<pre><code>

import time
def euler11():
  M = [[ 8, 2,22,97,38,15, 0,40, 0,75, 4, 5,07,78,52,12,50,77,91, 8],
       [49,49,99,40,17,81,18,57,60,87,17,40,98,43,69,48,04,56,62,00],
       [81,49,31,73,55,79,14,29,93,71,40,67,53,88,30, 3,49,13,36,65],
       [52,70,95,23, 4,60,11,42,69,24,68,56, 1,32,56,71,37, 2,36,91],
       [22,31,16,71,51,67,63,89,41,92,36,54,22,40,40,28,66,33,13,80],
       [24,47,32,60,99, 3,45,02,44,75,33,53,78,36,84,20,35,17,12,50],
       [32,98,81,28,64,23,67,10,26,38,40,67,59,54,70,66,18,38,64,70],
       [67,26,20,68, 2,62,12,20,95,63,94,39,63, 8,40,91,66,49,94,21],
       [24,55,58, 5,66,73,99,26,97,17,78,78,96,83,14,88,34,89,63,72],
       [21,36,23, 9,75, 0,76,44,20,45,35,14, 0,61,33,97,34,31,33,95],
       [78,17,53,28,22,75,31,67,15,94, 3,80, 4,62,16,14, 9,53,56,92],
       [16,39, 5,42,96,35,31,47,55,58,88,24, 0,17,54,24,36,29,85,57],
       [86,56, 0,48,35,71,89, 7, 5,44,44,37,44,60,21,58,51,54,17,58],
       [19,80,81,68, 5,94,47,69,28,73,92,13,86,52,17,77, 4,89,55,40],
       [ 4,52, 8,83,97,35,99,16, 7,97,57,32,16,26,26,79,33,27,98,66],
       [88,36,68,87,57,62,20,72, 3,46,33,67,46,55,12,32,63,93,53,69],
       [ 4,42,16,73,38,25,39,11,24,94,72,18, 8,46,29,32,40,62,76,36],
       [20,69,36,41,72,30,23,88,34,62,99,69,82,67,59,85,74, 4,36,16],
       [20,73,35,29,78,31,90, 1,74,31,49,71,48,86,81,16,23,57, 5,54],
       [ 1,70,54,71,83,51,54,69,16,92,33,48,61,43,52, 1,89,19,67,48]]
       
  max_product = 0
  for i in xrange(20):
    for j in xrange(16):
      """
      Side to side
      """
      product = slice_product(M[i][j:j+3])
      #product = M[i][j] * M[i][j + 1] * M[i][j + 2] * M[i][j + 3]
      if product > max_product: max_product = product
    
      """
      Up and down
      """
      M = zip(*M) #transposes the list so we can get at the columns
      product = slice_product(M[i][j:j+3])
      #product = M[j][i] * M[j + 1][i] * M[j + 2][i] * M[j + 3][i]
      if product > max_product: max_product = product
 
      #diagonal products
      if i < 16:
        product = slice_product([M[i+n][j+n] for n in range(4)])
        #product = M[i][j] * M[i + 1][j + 1] * M[i + 2][j + 2] * M[i + 3][j + 3]
        if product > max_product: max_product = product
      
      if i > 2:
        product = slice_product([M[i-n][j+n] for n in range(4)])
        #product = M[i][j] * M[i - 1][j + 1] * M[i - 2][j + 2] * M[i - 3][j + 3]
        if product > max_product: max_product = product
      
  return max_product

#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler11())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))

if __name__ == '__main__':
  main()