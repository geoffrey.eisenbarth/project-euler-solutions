#!/usr/local/bin/python2.7

import time
import itertools
import prime

def euler27():
  """
  Find the product of the coefficients, a and b, for the quadratic expression that produces 
  the maximum number of primes for consecutive values of n, starting with n = 0.
  """
  
  solution = (0, 0, 0)
  #Note: b must be prime for n = 0 to result in a prime. 
  for a, b in itertools.product(range(-999, 1000), prime.prime_list(1000)):
    n = 0
    while True:
      if not prime.is_prime(n ** 2 + a * n + b):
        break
      elif n > solution[2]:
        solution = (a, b, n + 1) #the n + 1 is to account for number of primes including n = 0 
        n += 1
      else:
        n += 1
        
  return solution
        
#boilerplate code to run
def main():
  start = time.time()
  print 'The quadratic n^2 + %d n + %d generates %d consecutive primes' % euler27()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()