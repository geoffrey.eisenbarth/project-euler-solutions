#!/usr/local/bin/python2.7

import itertools, math, time
def sieve_of_atkin(ubound):
  """
  Returns a list of primes utilizing the Sieve of Atkin
  """
  running_sum = 5
  #initialize the sieve
  sieve = [False] * (ubound + 1)
  
  #put in candidate primes:
  #integers which have an odd number of
  #representations by certain quadratic forms
  for x, y in itertools.product(range(1, int(math.sqrt(ubound)) + 1), range(1, int(math.sqrt(ubound)) + 1)):
    n = 4 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 1 or n % 12 == 5):
      sieve[n] = not sieve[n]      
    
    n = 3 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 7):
      sieve[n] = not sieve[n]
        
    n = 3 * x ** 2 - y ** 2
    if (x > y) and (n <= ubound) and (n % 12 == 11):
      sieve[n] = not sieve[n]

  #eliminate composites by sieving
  for n in xrange(5, int(math.sqrt(ubound)) + 1):
    if sieve[n]:
      #n is prime, omit multiples of its square; this is
      #sufficient because composites which managed to get
      #on the list cannot be square-free
      for k in xrange(n ** 2, ubound, n ** 2):
        sieve[k] = False
  
  #list doesn't account for 2 and 3
  sieve[2] = True
  sieve[3] = True
  return [number for (number, is_prime) in enumerate(sieve) if is_prime]
  #return sieve

def euler10():
  """
  Solution to Euler10: Find the sum of all the primes below two million.
  """

  return sum(sieve_of_atkin(2000000))
  
  

#boilerplate code to run
def main():
  start = time.time()
  print 'The solution is %d' % euler10()
  print 'Time elapsed: ' + str((time.time() - start))
  
if __name__ == '__main__':
  main()

