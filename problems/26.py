#!/usr/local/bin/python2.7

import time
import itertools
import sys
import math
import prime

def euler26():
  """
  Find the value of d < 1000 for which 1/d contains the 
  longest recurring cycle in its decimal fraction part.
  """
  
  #Repeating decimals for (1/d) will happen only when the denominator is prime (with the
  #exception of 2 and 5, which are co-prime with 10). The length of the repetend of 1/d 
  #is equal to the order of 10 mod d, where the order of 10 mod d is the smallest positive 
  #k with 10^k = 1 mod d.
  
  max_length = 0
  solution = (0, 0)
  for p in prime.prime_list(1000)[3:]: 
   i = 1
   while True:
     if 10 ** i % p == 1:
       order = i
       break
     else: i += 1
  
   if order > solution[1]:
    solution = (p, order)
  
  return solution
  

#boilerplate code to run
def main():
  start = time.time()
  print 'The length of the repetend of 1/%d is %d' % euler26()
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()