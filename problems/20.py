""""
<p>
<strong>Statement: </strong> \(n!\) means \(n \times (n − 1) \times \cdots \times 3 \times 2 \times 1\). For example,
\[10! = 10 \times 9 \times \cdots \times 3 \times 2 \times 1 = 3,628,800,\]
and the sum of the digits in the number \(10!\) is \(3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.\) Find the sum of the digits in the number \(100!\).
</p>
<hr />
<p>
This can be done in one line, but I split it into two in order to demonstrate Python's list comprehensions a little clearer. A list in Python is exactly what it sounds like, an ordered collection of objects. In Python, lists are denoted between square brackets, <tt>[]</tt>, so <tt>[1, 2, 'apple']</tt> would be a list containing the integers <tt>1</tt>, <tt>2</tt>, and the string <tt>'apple'</tt>. In Python, list can hold just about anything, even other lists! For instance
<pre style="text-align:center;"><code>
[1, 2, 'apple', ['inception', 6]]
</code></pre>
is a list containing the elements in the previous list, plus another list containing the string (<tt>'inception'</tt> and the integer <tt>6</tt>). These nested listed can go deeper than Leonardo DiCaprio and Christopher Nolan could ever imagine: Python allows for arbtirarily-many nested lists. 
</p>
<p>
However, these lists wouldn't be nearly as useful if there wasn't a way to build lists out of certain ingredients instead of typing them out everytime. That is exactly what Python list comprehensions do. In general, list comprehensions follow the synatx
<pre style="text-align:center;"><code>
[function(part) for part in iterable if logic].
</code></pre>
<p> 
We will be ignoring the <tt>if logic</tt> part in this lesson. Notice that <tt>factorial()</tt> function (which is imported from the module <tt>math</tt>) returns a numeric type, which can not be iterated through. Since we want to strip away each digit of the number, we convert the number to a string using <tt>str()</tt> and convert the 'stringed' version of each digit back to the integer type. That is, the syntax

<pre style="text-align:center;"><code>
[character for character in str(345)] 
</code></pre>
<p> 
will go from left to right in the string '345' and add each character to the list.This would produce the following list:
</p>
<pre style="text-align:center;"><code>
['3', '4', '5'].
</code></pre>
<p>
As a result, we need to convert each character back to an integer data type using the <tt>int()</tt> function.
</p>
<pre style="text-align:center;"><code>
[int(character) for character in str(345)], 
</code></pre>
<p>
which gives us our desired list: <tt>[3, 4, 5]</tt>. We can then use the built-in function <tt>sum()</tt> to quickly sum up all the elements in a list. This is done
below.
</p>
<pre><code>
""""
import time
from math import factorial

def euler():
  """
  Convert 100! to a string so that we can iterate through and create 
  a list containing each digit (must convert back to int type). We
  then return the sum of this list.
  """
  digits = [int(character) for character in str(factorial(100))]
  
  return sum(digits)
  
#boilerplate code to run
def main():
  start = time.time()
  print('The solution is: %d' % euler())
  elapsed = (time.time() - start)
  print('Time elapsed: ' + str(elapsed))
  
if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 648
Time elapsed: 0.000999
</pre>
"""