"""
<p>
<strong>Statement: </strong> Let \(d(n)\) be defined as the sum of the proper divisors of \(n\) (numbers less than \(n\) which divide evenly into \(n\)). If \(d(a) = b\) and \(d(b) = a\), where \(a \neq b\), then \(a\) and \(b\) are called an <em>amicable pair</em> and each of \(a\) and \(b\) are called <em>amicable numbers</em>.

For example, the proper divisors of \(220\) are 
\[1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110;\]
therefore \(d(220) = 284\). The proper divisors of \(284\) are 
\[1, 2, 4, 71, 142;\] 
so \(d(284) = 220\).

Evaluate the sum of all the amicable numbers under \(10,000\).
</p>
<hr />
<p>
Amicable numbers! These fun numbers are a generalization of <em>perfect numbers</em>, which are numbers \(a\) such that \(a = d(a)\), where \(d(a)\) is defined as above. Mathematicians have even extended the 'amicable' analogy to 'sociable numbers,' which are numbers \(a, b, c, \dots, n\) for which 
\[d(a) = d(b) = \dots = d(n) = d(a).\]
<p>
Determining whether a given pair of numbers are amicable can be easily coded provided you know a bit more about the structure of amicable numbers. While there are no known formulas that generate <em>all</em> amicable numbers, there exist (quite old) methods for discovering amicable numbers: particularly a <a href="https://en.wikipedia.org/wiki/Amicable_numbers#Th.C4.81bit_ibn_Qurra_theorem">theorem</a> by Arab mathematician <a href="https://en.wikipedia.org/wiki/Th%C4%81bit_ibn_Qurra">Thabit ibn Qurra</a> (826 - 901) and a <a href="https://en.wikipedia.org/wiki/Amicable_numbers#Euler.27s_rule">generalization</a> of said theorem by <a href="https://en.wikipedia.org/wiki/Leonhard_Euler">Euler</a> in 1750. Unfortunately, since neither of these results claim to provide <em>every</em> amicable pair, they are of little immediate use to us in solving this problem.
</p>
<p>
Instead, lets dissect amicable numbers ourselves and see what we can determine about them that might be useful in our code. Given a number \(a\), if \(a\) is prime, then the sum of all proper divisors of \(a\), denoted \(d(a)\), would be \(1\). It is impossible (by definition) for \(1\) to be an amicable number since it is a perfect number. Thus, we don't have to check any prime numbers to determine if they are part of an amicable pair. If we find an \(a\) such that \(d(a)\) is <em>not</em> prime, then it remains to see if \(d(d(a)) = a\). Due to the fact that our primality test runs quicker than the calculation of \(d(a)\), we will also check to see if \(d(d(a))\) is prime before checking if \(d(d(a)) = a\). 
</p>
<p>
It has also been <a href="https://en.wikipedia.org/wiki/Amicable_numbers">shown</a> that all currently discovered amicable pairs (which far exceed the 10,000 we're looking at) are known to 1) either be both even or both odd, and 2) share a common factor greater than 1. We can use these criterion to shrink down the pool of possible amicable numbers in our loop, although in this situation we discard 2) due to the extra calculations involved in computing a list of divisors multiple times.
</p>
<p>
I stumbled across a much more efficient primality test online recently that takes advantage of the Python language. Instead of using <tt>xrange()</tt> to run through the numbers from \(1\) to \(\sqrt{n}\) (which would crash for numbers \(n > 2^{31} - 1\) (the maximum C <tt>long</tt> value), we use two functions from the <tt>itertools</tt> package: <tt>count()</tt> and <tt>islice()</tt>, as well as Python's built in <tt>all()</tt>. <tt>count(n)</tt> is an iterator that produces all the numbers from \(n\) to infinity, bypassing the potential problem of checking the primality of a number that is larger than the C <tt>long</tt> data type allows. We combine this with <tt>islice(<em>iterable, stop</em>)</tt> which returns an iterable with an upperbound. Finally, Python's built in <tt>all()</tt> function returns <tt>True</tt> if all elements in an iterable evaluate to <tt>True</tt>. Since <tt>0</tt> evaluates to <tt>False</tt>, we can use <tt>all()</tt> to determine if any numbers from our iterator evenly divide our candidate. This is shown below.
</p>
<pre><code>
"""
from itertools import count, islice
from math import sqrt

def is_prime(n):
  if n < 2: return False
  return all(n % i for i in islice(count(2), int(sqrt(n) - 1)))
"""
</code></pre>
<p>
Using this primality test, we can generate a list of all proper divisors. Again, we only have to check up to \(\sqrt{n}\) if we include both the divisor \(i\) and \(\frac{n}{i}\) as a tuple. We then flatten out the list of tuples and append the number \(1\) (which isn't picked up by our test). If our input \(n\) is a square, say \(100\), then one of the tuples will be <tt>(10, 10)</tt>, so we discard duplicates by throwing the list through <tt>set()</tt>. Once this function is coded, coding \(d(n)\) is straightforward.
</p>
<pre><code>
"""
def proper_divisors(n):
  # get pairs of all divisors except 1 and n
  pairs = [(i, n / i) for i in islice(count(2), int(sqrt(n) - 1)) if n % i == 0]
  
  # flatten to list
  divisors = list(sum(pairs, ()))
  
  #add 1 and remove duplication if n is square
  return [1] + list(set(divisors))
  
def d(n):
  return sum(proper_divisors(n))
"""
</code></pre>
<p>
We now loop through the numbers from \(2\) to \(10,000\), throwing away any numbers that have already been cached as amicable numbers, as well as numbers for which \(a\) and \(d(a)\) are not both even or both odd (and numbers for which \(a = d(a)\)). After weeding those out, we check the primality of \(a\) and \(b\) (since our primaility test is quicker than calculating \(d(b)\)), then finally determine if they are amicable (i.e., if \(d(b) = a\)). Since Python short-circuits logical evaluations (that is, doesn't bother "reading the whole thing" once the logic has been satisfied), we order the statements taking in consideration the loop efficiency. Once an amicable pair has been detected, they are cached and added to the talley. 
<p>
<pre><code>
"""
import time

def euler21(upper_bound):
  
  tally = 0 
  amicable_numbers = []
  
  for a in xrange(2, 10000):
    b = d(a)
    if a % 2 == b % 2 and not a in amicable_numbers and a != b \
    and not is_prime(a) and not is_prime(b) and d(b) == a:
        amicable_numbers += [a, b]
        tally += a + b

  return tally

# boilerplate code to run
def main():
  start = time.time()
  print 'The solution is: %d' % euler21(10000)
  elapsed = (time.time() - start)
  print 'Time elapsed: ' + str(elapsed)  
  
if __name__ == '__main__':
  main()
"""
</code></pre>
<hr>
<pre>
The solution is: 31626
Time elapsed: 0.171999
</pre>
"""