import math
import itertools
import operator
import sys
import time
import random


def gcd(a, b):
  if b == 0:
    return a
  else:
    return gcd(b, a % b)

def lcd(a, b):
  return (abs(a) / gcd(a, b) ) * abs(b)


def modular_power(base, exponent, modulus):
  """
  More efficient way of computing base ** exponent % modulus.
  """
  c = 1
  for e_prime in xrange(1, exponent):
    c = (c * base) % modulus
  return c
  
def totient(n):
  """
  We utilize Euler's totient formula, where the totient of n is equal to n times
  the product of (1 - 1/p), where p ranges over the prime divisors of n. These
  are given to us by the first element of the tuples from the prime_factor_list. 
  """
  
  return int(n * reduce(operator.mul, map(lambda x : 1.0 - 1.0 / x[0], prime_factor_list(n))))

def fermat_prime_test(n, k):
  """
  Use Fermat Primality Test to see if n is prime, done with k many iterations
  """
  count = 1
  while count < k:
    a = random.randint(1, n - 1)
    if modular_power(a, (n - 1), n) != 1:
      return False
    count += 1
  
  return True

"""
def miller_rabin_test(n):
"""

def is_prime(n):

  #special cases 2 and 3, excludes (-infty, 1]
  if n <= 3:
    return n >= 2  
  if n % 2 == 0 or n % 3 == 0 or n % 5 == 0:
    return False
  #Since all primes except 2 and 3 are of the form p = 6k +/- 1, 
  #we look for prime divisors of this form. Note, that just because 
  #a number has the form 6k +/- 1 does not mean it will be prime, but
  #we are sure to pick up all primes less than ubound with this
  #approach. 
  
  k = 1
  while 6 * k + 1 <= int(math.sqrt(n)) + 1:
    if n % (6 * k + 1) == 0 or n % (6 * k - 1) == 0:
      return False
    k += 1
  
  return True
  
def prime_list(ubound):
  """
  Returns a list of primes utilizing the Sieve of Atkin
  """
  #initialize the sieve
  primes = [2, 3]
  sieve = [False] * (ubound + 1)
  
  #put in candidate primes:
  #integers which have an odd number of
  #representations by certain quadratic forms
  for x, y in itertools.product(range(1, int(math.sqrt(ubound)) + 1), range(1, int(math.sqrt(ubound)) + 1)):
    n = 4 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 1 or n % 12 == 5):
      sieve[n] = not sieve[n]
    
    n = 3 * x ** 2 + y ** 2
    if (n <= ubound) and (n % 12 == 7):
      sieve[n] = not sieve[n]
      
    n = 3 * x ** 2 - y ** 2
    if (x > y) and (n <= ubound) and (n % 12 == 11):
      sieve[n] = not sieve[n]
      
  #eliminate composites by sieving
  for n in xrange(5, int(math.sqrt(ubound)) + 1):
    if sieve[n]:
      #n is prime, omit multiples of its square; this is
      #sufficient because composites which managed to get
      #on the list cannot be square-free
      for k in xrange(n ** 2, ubound, n ** 2):
        sieve[k] = False
  
  #list doesn't account for 2 and 3
  sieve[2] = True
  sieve[3] = True
  return [n for n, e in enumerate(sieve) if e]
  #return sieve
  
def prime_sum(ubound):

  #Sieve doesn't pick up 2 and 3
  return 2 + 3 + reduce(lambda x, y: x + y, prime_list(ubound)) 
  
def prime_factor_list(ubound):
  factors = {}
  
  #go through the primes in reverse order and
  #determine their powers in prime factorization
  for i in prime_list(ubound)[::-1]:
    factors[i] = 0          #exponent associated with each prime factor
    reduce = ubound
    while reduce % i == 0:
      reduce = reduce / i
      factors[i] += 1
  
  #return all tuples (factor, exponent) for which exponent != 0
  return [(p, factors[p]) for p in factors if factors[p] != 0]
  
def divisor_generator(ubound):
    factors = prime_factor_list(ubound)
    nfactors = len(factors)
    f = [0] * nfactors
    while True:
        yield reduce(lambda x, y: x*y, [factors[x][0]**f[x] for x in range(nfactors)], 1)
        i = 0
        while True:
            f[i] += 1
            if f[i] <= factors[i][1]:
                break
            f[i] = 0
            i += 1
            if i >= nfactors:
                return
                
def divisor_list(ubound):
    divisors = []
    factors = prime_factor_list(ubound)
    nfactors = len(factors)
    f = [0] * nfactors
    while True:
        divisors.append(reduce(lambda x, y: x*y, [factors[x][0]**f[x] for x in range(nfactors)], 1))
        i = 0
        while True:
            f[i] += 1
            if f[i] <= factors[i][1]:
                break
            f[i] = 0
            i += 1
            if i >= nfactors:
                return sorted(divisors)
                
def proper_divisor_list(ubound):
    divisors = []
    factors = prime_factor_list(ubound)
    nfactors = len(factors)
    f = [0] * nfactors
    while True:
        divisors.append(reduce(lambda x, y: x*y, [factors[x][0]**f[x] for x in range(nfactors)], 1))
        i = 0
        while True:
            f[i] += 1
            if f[i] <= factors[i][1]:
                break
            f[i] = 0
            i += 1
            if i >= nfactors:
                return sorted(divisors)[:-1]

